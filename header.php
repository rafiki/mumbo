<?php require_once('data.php') ?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="main.css" />
		<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<script src="js/main.js" type="text/javascript"></script>
		<title>Tufts MUMBO</title>
	</head>
	
	<body>
		
		<div id="header">
			<div id="banner">
				<a href="home.html">			
					<img src="images/jumbotufts.gif" />
					<h1 id="banner-title">
							Welcome to mumbo.tufts.edu!
					</h1>
				</a>
				<div style="clear: both; display: blocked;"></div>
			</div>
			<div id="menu">
				<?php if ($_SERVER["PHP_SELF"] != "/mumbo/index.php") { ?>
				<a href="home.php"><p class="menu-item">Home</p></a>
				<a href="index.php"><p class="menu-item" style="float: right">Logout</p></a>
				<?php } else { ?>
				<br />
				<?php } ?>
				<div style="clear: both; display: blocked;"></div>				
			</div>
			<div style="clear: both; display: blocked;"></div>		
		</div>
		
		<div id="main-container">