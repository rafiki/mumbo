<?php require_once('header.php')	?>
		<?php 
			$id      = $_GET['class_id'];
			$class   = $classroom[$id];
			$teacher = $user[$class['teacher_id']];
		?>
		<div id="container">
			<div id="content">
				<div id="classroom">
					<h1><?php echo $class['title'] ?></h1>
					<?php
						if ( !empty($teacher) ) {
							echo '<img src="images/' . $teacher['image'] . '" height="72" width="72" \>';
						}
					?>
					<br />
					<h3>&nbsp;
						<a href="user.php?user_id=<?php echo $class['teacher_id']; ?>">
						<?php echo $teacher['title'] . ' ' . $teacher['fname'] . ' ' . $teacher['lname'] ?>
						</a>
					</h3>
					<div id="motd">
						<?php echo $class['desc'] ?>
					</div>
						<?php
							if ( count($class['assignments']) > 0 ) {
								echo '<h4>Assignments:</h4>';
								echo '<ul>';
								foreach ($class['assignments'] as $key => $assgn_id) {
								 echo '<li>';
								 	echo '<a href="assignment.php?assgn_id=' . $assgn_id .'">'; 
								 		echo $assignment[$assgn_id]['title'];
								 	echo '</a>';
								 echo '</li>';
								}
								echo '</ul>';
							}
						?>						
				</div><!-- end classroom div -->
			</div><!-- end content div -->			
			<div style="clear: both; display: blocked;"></div>
		</div>	<!-- end container div -->	
<?php require_once('slider.php') ?>
<?php require_once('footer.php')	?>