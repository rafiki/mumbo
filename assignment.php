<?php 
require_once('header.php');
$id = $_GET['assgn_id'];
$assgn = $assignment[$id]; 
$cls_id = $assgn['class_id']; 
$class = $classroom[$cls_id];
$cls_name_parts = explode(':', $class['title']);
$cls_name = $cls_name_parts[0];
?>
  <div id="container">
   <div id="content">
       <div id="assignment">
			    	<h1><?php echo($assgn['title']); ?></h1>
				    <?php echo($assgn['desc']); ?>
				    <div id="assignmentForm">
					    Submission Title <br />
					    <input type="text" style="width: 90%;" /> <br />
					    
					    Content
					    <br/>
					    <textarea cols="30" rows="10" style="width: 90%;"></textarea>
		
					    <br/>
					    <p>Upload an attachment: <input type="file"></p>
		
					    <button id="submitAssignment" type="button"/>Submit</button>
				    </div>
							</div>
   
   </div><!-- end content div -->   
   <div style="clear: both; display: blocked;"></div>
  </div> <!-- end container div --> 

<?php require_once('slider.php') ?>
<?php require_once('footer.php') ?>