/* Rafi Yagudin and Paul Nixon
 * Comp 171
 * A5
 *
 * JavaScript to handle logging in and other various 
 * site features.
 */

$(document).ready(function(){
	
	/* Validate log */
	$('#login-button').click(function(){
		var username = $('#username').val();
		var password = $('#password').val();

		if(username == '' || username == undefined){
			alert('Please fill in a username!');
			$('#username').focus();
		}else if(password == '' || password == undefined){
			alert('Please fill in a password!');
			$('#password').focus();
		}else {
			localStorage.setItem('username', username);
			window.location.href = "home.php";
		}
	})
	
	/* Fix slider height */
	var sliderMaxHieght = $('#container').height();
	$('#connect-slider').css('height', sliderMaxHieght)
	
	$('#submitAssignment').click(function(){
		$('#assignmentForm').html('Thank you for your submission!');
	});
	
})
