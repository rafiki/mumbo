<?php require_once('header.php')	?>
		<div id="container">
			<div id="content">
					<h3>Your Classes:</h3>
					<?php 
						 foreach ($classroom as $id => $class_data) {
					?>
							<div class="class" id="<?php echo $id; ?>">
									<h3><a href="classroom.php?class_id=<?php echo $id ?>"><?php echo $class_data['title']; ?></a></h3>
									<?php
										if ( count($class_data['assignments']) > 0 ) {
											echo '<h4>Assignments:</h4>';
											echo '<ul>';
											foreach ($class_data['assignments'] as $key => $assgn_id) {
											 echo '<li>';
											 	echo '<a href="assignment.php?assgn_id=' . $assgn_id .'&class_id=' . $assignment[$assgn_id]['class_id']. '">'; 
											 		echo $assignment[$assgn_id]['title'];
											 	echo '</a>';
											 echo '</li>';
											}
											echo '</ul>';
										}
									?>
									
									<?php
										if ( count($class_data['events']) > 0 ) {
											
											echo '<h4>Events:</h4>';
											echo '<ul>';
											foreach ($class_data['events'] as $event_id => $event_data) {
											 echo '<li>';
											 	echo '<a href="event.php?event_id=' . $event_id .'">'; 
											 		echo $event[$event_id]['title'];
											 	echo '</a>';
											 	echo '<br />';
											 		if (count($event[$event_id]['attending']) > 0) {
											 			echo '<p>People attending:</p>';
											 			foreach($event[$event_id]['attending'] as $key => $usr_id) {													
											 				echo '<a href="user.php?user_id=' . $usr_id . '" title="' . $user[$usr_id]['fname'] . ' ' . $user[$usr_id]['lname'] . '">';
												 				echo '<img src="images/' . $user[$usr_id]['image'] . '" height=32 width=32 />';
												 			echo '</a>&nbsp;';
											 			}
											 		}
											 echo '</li>';
											 
											}
											echo '</ul>';
										}
									?>	
																		
							</div>
					<?php	
					  }
					 ?>
			</div><!-- end content div -->			
			<div style="clear: both; display: blocked;"></div>
		</div>	<!-- end container div -->	

<?php require_once('slider.php') ?>
<?php require_once('footer.php')	?>