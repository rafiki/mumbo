<?php
 /**** Users ****/
 
 //Professor Awesome
 $user[0] = array('fname'       => 'Totally',
                  'lname'       => 'Awesome',
                  'title'       => 'Professor',
                  'abbrTitle'   => 'Prof',
                  'connections' => array('You are currently taking Prof. Awesome\'s Computer Insecurity course', 'You took Prof. Awesome\'s Googling course in Fall 2010'),
                  'teaches'     => array(0, 1),
                  'enrolled'    => array(),
                  'image'       => 'professor_awesome.jpg' 
                 );

 //Professor Snoop Lion
 $user[1] = array('fname'       => 'Snoop',
                  'lname'       => 'Lion',
                  'title'       => 'Professor',
                  'abbrTitle'   => 'Prof',
                  'connections' => array('You took Prof. Snoop Lion\'s Gardening 250 course in Spring 2011'),
                  'teaches'     => array(),
                  'enrolled'    => array(),
                  'image'       => 'snoop_lion.jpg'
                  );
 
 //Brad Pitt               
 $user[2] = array('fname'       => 'Brad',
                  'lname'       => 'Pitt',
                  'title'       => 'Instructor',
                  'abbrTitle'   => '',
                  'connections' => array('You are currently taking Instructor Brad Pitt\'s Fight Club 250'),
                  'teaches'     => array(2),
                  'enrolled'    => array(0),
                  'image'       => 'brad_pitt.jpg'
                  );

 //Angelina Jolie
 $user[3] = array('fname'       => 'Angelina',
                  'lname'       => 'Jolie',
                  'title'       => '',
                  'abbrTitle'   => '',
                  'connections' => array('You and Angelina are both in the same major', 'You and Angelina took Chem 101 together in Fall 2012'),
                  'teaches'     => array(),
                  'enrolled'    => array(0, 1, 2),
                  'image'       => 'angelina_jolie.jpg'
                  );
 
 //Cookie Monster                 
 $user[4] = array('fname'       => 'Cookie',
                  'lname'       => 'Monster',
                  'title'       => '',
                  'abbrTitle'   => '',
                  'connections' => array('You and the Cookie Monster both like cookies!'),
                  'teaches'     => array(),
                  'enrolled'    => array(0, 1),
                  'image'       => 'cookie_monster.jpg'
                 );

 //Bill Gates                 
 $user[5] = array('fname'       => 'Bill',
                  'lname'       => 'Gates',
                  'title'       => '',
                  'abbrTitle'   => '',
                  'connections' => array('You took and Bill Weightlifting 250 together in Fall 2012'),
                  'teaches'     => array(),
                  'enrolled'    => array(1,2),
                  'image'       => 'bill_gates.jpg'
                 );

		/**** Classrooms ****/
	
		//Comp 188: Computer Insecurity
		$classroom[0] =  array('title'       => 'Comp 188: Computer Insecurity',
																				     'desc'        => '<p>This class will teach you how to write terrible code and blame others.</p>'.
																					 			    							      '<p>Class is cancelled Wednesday Apr. 12.</p>',
																				     'assignments' => array(0),
																			 	    'events'      => array(),
																			 	    'teacher_id'  => 0
																		      );
	 
	 //Fight Club 250
		$classroom[1]	 = array('title'       => 'Fight Club 250: Soapmaking and Mayhem',
																	 			    'desc'        => '<p>1st rule: You do not talk about fight club.</p>' .
																		 						    													'<p>2nd rule: You DO NOT talk about fight club.</p>' .
																			 												    	     '<p>3rd rule: You know what? Just don\'t talk about anything while the professor is talking.</p>' .
																				 								   				 					'<p>Class is cancelled Wednesday Apr. 12.</p>',
																		       'assignments' => array(1),
																		       'events'      => array(0),
																		       'teacher_id'  => 2
																		 );
																		 
  //Comp 214: Martial Arts for Computer Scientists
  $classroom[2]  = array('title'       => 'Comp 214: Martial Arts for Computer Scientists',
                         'desc'        => '<p>Beat people over the head, with Unix pipes.</p>' .
                                          '<p>Coerce types, using threats of force.</p>' .
                                          '<p>Build extra-strong handshakes into your protocols.</p>',
																		       'assignments' => array(2,3),
																		       'events'      => array(),
																		       'teacher_id'  => 0
																		      );


  /**** Assignments ****/

  //Comp 188: Assignment 3
  $assignment[0] = array('title'       => 'Comp 188: Assignment 3',
                         'desc'        => '<h3>Due tonight at 11:59PM</h3><br/>' .
                                          '<p>Assignment: Describe how a Turboencabulator can be dismantled and then remantled.</p>' .
                                          '<p>You may refer only to the official GE turboencabulator reference sheet.</p>' .
                                          '<p>Resources: <a href="http://upload.wikimedia.org/wikipedia/en/3/36/GE_Turboencabulator_pg_1.jpg">Reference sheet</a></p>',
                         'class_id'    => 0
                        );

		//Fight Club 250: Assignment 1	 																	
	 $assignment[1] = array('title'       => 'Fight Club 250: Assignment 1',
																					    'desc'        => '<p>Don\'t break the rules of Fight Club.</p>',
	                        'class_id'    => 1
	 																	     );
	 																	     
		//Comp 214: Assignment 1																	
	 $assignment[2] = array('title'       => 'Comp 214: A1',
																					    'desc'        => '<p>Practice the art of judo-typing.</p>',
	                        'class_id'    => 2
	 																	     );
	 																	     
		//Comp 214: Assignment 2																	
	 $assignment[3] = array('title'       => 'Comp 214: A2',
																					    'desc'        => '<p>Master the art of karate-debugging.</p>',
	                        'class_id'    => 2
	 																	     );	 																	     
	 
	 /**** Events ****/
  
  //Fight Club 250 event
  $event[0]      = array('title'       => 'Fight Club Session 3',
                         'attending'   => array(0,1,2)
                        );
  
?>