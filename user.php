<?php 
require_once('header.php');
$user_id = $_GET['user_id'];
$thisuser = $user[$user_id]; 
?>
  <div id="container">

   <div id="content">

    <div id="person">
       <h1><?php echo($thisuser['title'].' '.$thisuser['fname'].' '.$thisuser['lname']); ?></h1>
       <br/>
       <img src="<?php echo('images/'.$thisuser['image']); ?>" width="170" height="170"/>
       <!--<h3>Acting, Class of 2013</h3>-->
       <?php if(count($thisuser['enrolled']) > 0) { ?>
         <h3>Enrolled:</h3>
         <ul>
         <?php foreach($thisuser['enrolled'] as $classnum){ ?>
           <li><?php echo('<a href="classroom.php?class_id='.$classnum.'">'.$classroom[$classnum]['title'].'</a>'); ?></li>
         <?php } ?>              
         </ul>
       <?php } ?>
       <?php if(count($thisuser['teaches']) > 0) { ?>
         <h3>Teaches:</h3>
         <ul>
         <?php foreach($thisuser['teaches'] as $classnum){ ?>
           <li><?php echo('<a href="classroom.php?class_id='.$classnum.'">'.$classroom[$classnum]['title'].'</a>'); ?></li>
         <?php } ?>              
         </ul>
       <?php } ?>
       <h3>Connections:</h3>
       <ul>
       <?php foreach($thisuser['connections'] as $connection){ ?>
         <li><b><?php echo($connection); ?></b></li>         
       <?php } ?>
       </ul>
   </div>
   
   </div><!-- end content div -->   
   <div style="clear: both; display: blocked;"></div>
  </div> <!-- end container div --> 

<?php require_once('slider.php') ?>
<?php require_once('footer.php') ?>
