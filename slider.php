	<?php 
		$slider_usrs = array();
		if ($_GET['class_id'] != NULL) {
			$class_id    = $_GET['class_id'];	
			foreach($user as $usr_id => $usr_info){
				if(in_array($class_id, $usr_info['enrolled'])){
					array_push($slider_usrs, $usr_id);
				}
			}
		} else {
			foreach($user as $usr_id => $usr_info){
					array_push($slider_usrs, $usr_id);
			}
		}
	?>
		<div id="connect-slider">
			<div id="slider">
			<p>Connect with your fellow colleagues:</p>
				<?php foreach($slider_usrs as $key => $usr_id) { ?>
				<div class="slider-profile" id="<?php echo 'usr-' . $usr_id ?>">
					<a href="user.php?user_id=<?php echo $usr_id ?>" title="<?php echo implode(' ', $user[$usr_id]['connections']) ?>">
						<img class="slider_pic" src="images/<?php echo $user[$usr_id]['image'] ?>" />
						<p><?php echo $user[$usr_id]['title'] . ' ' . $user[$usr_id]['fname'] . ' ' . $user[$usr_id]['lname'] ?></p>
					</a>
				</div>
				<?php } ?>
				<div style="clear: both; display: blocked;"></div>
			</div>
		</div> <!-- end slider div -->